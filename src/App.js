import React, { Component } from 'react';

import Form from './Components/Form/Form.jsx';
import './App.css';
import './styles/commons.css';
import './styles/reset.css';

class App extends Component{
  render(){
    return(
        <div className="app-container">
          <div className="app">
                <Form/>
          </div>
        </div>
    )
  }

}





export default App;